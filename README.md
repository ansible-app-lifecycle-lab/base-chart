# base Chart

Helm Chart provided by Operations Team for Kubernates applications.

This helm chart will help organization development teams to deploy applications in OpenShift in a compliance and secure way.

## Configuration

In order to deploy an application pass this chart a `values.yaml` file with the following configuration options:

<table>
<tr><th>Property</th><th>Description</th><th>Example</th></tr>
<tr><td>global.nameOverride</td><td>Override application Name</td><td>newName</td></tr>
<tr><td>runtime</td><td>Runtime OCP topology label</td><td>base</td></tr>
<tr><td>size</td><td>Size of the application (S). S is the default and only available value</td><td>M</td></tr>
<tr><td>image.name</td><td>Application image</td><td>openshift/appName</td></tr>
<tr><td>image.tag</td><td>Application image tag</td><td>1.0.0</td></tr>
<tr><td>extraLabels</td><td>Application resources extra labels</td><td>app.kubernetes.io/part-of: main-app</td></tr>
<tr><td>deploy.replicas</td><td>Number of pod replicas</td><td>1</td></tr>
<tr><td>deploy.strategy</td><td>Deployment strategy</td><td>RollingUpdate</td></tr>
<tr><td>deploy.annotations</td><td>Deployment annotations</td><td>app.openshift.io/connects-to: [{"apiVersion":"apps/v1","kind":"Deployment","name":"app-db"}]</td></tr>
<tr><td>deploy.ports</td><td>Application exposed ports</td><td>

```yaml
- name: http
  port: 8080
  targetPort: 8080
  protocol: TCP
```
</td></tr>
<tr><td>deploy.livenessProbe</td><td>Application liveness prove</td><td>

```yaml
failureThreshold: 3
httpGet:
  path: /q/health/live
  port: 8080
  scheme: HTTP
periodSeconds: 10
successThreshold: 1
timeoutSeconds: 1
```
</td></tr>
<tr><td>deploy.readiness</td><td>Application readiness prove</td><td>

```yaml
failureThreshold: 3
httpGet:
  path: /q/health/ready
  port: 8080
  scheme: HTTP
periodSeconds: 10
successThreshold: 1
timeoutSeconds: 1
```
</td></tr>
<tr><td>deploy.env</td><td>Application environment variables</td><td>

```yaml
- name: VAR_NAME
  value: var-value
```
</td></tr>
<tr><td>deploy.route.enabled</td><td>Expose application using a route</td><td>true</td></tr>
<tr><td>deploy.route.targetPort</td><td>Route target port</td><td>http</td></tr>
<tr><td>deploy.route.tls.enabled</td><td>TLS route</td><td>false</td></tr>
<tr><td>deploy.secret.name</td><td>Application secret name</td><td>RollingUpdate</td></tr>
<tr><td>deploy.secret.values</td><td>Application secret values list</td><td>

```yaml
- name: ADMIN_PASS
  value: HQ06MD0=
```
</td></tr>
<tr><td>volume.name</td><td>Volume name</td><td>app-data</td>
<tr><td>volume.path</td><td>Volume path</td><td>/var/lib/data</td>
<tr><td>volume.storage</td><td>Volume storage</td><td>1Gi</td>
<tr><td>volume.storageClassName</td><td>Volume storage class name</td><td>gp2</td>
<tr><td>configMap.injectEnv</td><td>Inject environment variable as a config file</td><td>true/false</td>
<tr><td>configMap.name</td><td>Configuration map mode</td><td>app-config</td>
<tr><td>configMap.path</td><td>Configuration map mount path</td><td>/app/config</td>
<tr><td>configMap.data</td><td>Configuration data</td><td>config.json: {"key": "value"}</td>
</table>

## Configuration file example

```yaml
global:
  nameOverride: app-name

runtime: spring-boot

size: S

image:
  name: quay.io/group/app
  tag: 1.0.0

extraLabels:
  app.kubernetes.io/part-of: app
  
deploy:
  replicas: 1

  strategy: RollingUpdate

  annotations:
    app.openshift.io/connects-to: >-
      [{"apiVersion":"apps/v1","kind":"Deployment","name":"app-db"}]

  ports:
    - targetPort: 8080
      protocol: TCP
      name: http
      port: 8080

  livenessProbe:
    failureThreshold: 3
    httpGet:
      path: /health/live
      port: 8080
      scheme: HTTP
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1

  readinessProbe:
    failureThreshold: 3
    httpGet:
      path: /health/ready
      port: 8080
      scheme: HTTP
    periodSeconds: 10
    successThreshold: 1
    timeoutSeconds: 1

  route:
    host: app.cluster.domain
    enabled: true
    targetPort: http
    tls:
      enabled: false
  
  env:
    - name: VAR_NAME
      value: var-value

  configMap:
    injectEnv: false
    name: config
    path: /usr/config
    data: |-
      config.json: |-
          { 
            "server": "/servers"
          }  

  volume:
    name: app-data
    path: /var/lib/data
    storage: 1Gi
    storageClassName: gp2
    volumeMode: Filesystem

secret:
  name: app-api-config
  values:
    - name: ADMIN_PASS
      value: SDDN2RjRmY1JPcXd6MD0=
```

## Run Tests

There are multiple test deployment scenarios ready to use in `test` folder. In order to deploy them follow these commands:

```sh
# Create a namespace
oc new-project base-chart-test

# Install helm test scenario
helm install scenario1 chart -f test/scenario1/values.yaml --wait -n base-chart-test

# Validate everything is deployed as expected

# Uninstall helm test scenario
helm uninstall scenario1 --wait -n base-chart-test

# Delete namespace
oc delete project base-chart-test
```

## Nexus Upload

Follow below instructions to upload the helm chart to Nexus:

```sh
# Create helm repository in Nexus if not exists:
curl -X 'POST' -v \
  -u <user>:<password> \
  '<nexus_url>/service/rest/v1/repositories/helm/hosted' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "helm",
  "online": true,
  "storage": {
    "blobStoreName": "default",
    "strictContentTypeValidation": true,
    "writePolicy": "allow_once"
  },
  "cleanup": {
    "policyNames": [
      "string"
    ]
  },
  "component": {
    "proprietaryComponents": true
  }
}'

# Package chart 
helm package chart

# Upload to Nexus
curl -u <user>:<password> -v \
  <nexus_url>/repository/helm/ \
  --upload-file base-chart-0.0.1.tgz
```
