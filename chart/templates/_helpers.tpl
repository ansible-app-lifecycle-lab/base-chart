{{- define "base.name" -}}
{{ default .Release.Name .Values.global.nameOverride }}
{{- end -}}

{{- define "base.labels" -}}
{{- $size := default "S" .Values.size -}}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version}}
{{- with .Values.extraLabels }}
{{ toYaml .}}
{{- end }}
{{ include "base.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
{{- if .Values.runtime }}
app.openshift.io/runtime: {{ .Values.runtime }}
{{- end }}
app.size: {{ $size }}
{{- end }}

{{- define "base.selectorLabels" -}}
app: {{ include "base.name" . }}
app.kubernetes.io/name: {{ include "base.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{- define "base.imageName" -}}
{{ default (include "base.name" .) .Values.image.name }}:{{ .Values.image.tag }}
{{- end -}}

{{- define "base.resources" -}}
{{- $size := default "S" .Values.size -}}
resources:
{{- if eq $size "S" }}
  limits:
    cpu: 100m
    memory: 256Mi
  requests:
    cpu: 100m
    memory: 256Mi
{{- else }}
  limits:
    cpu: 100m
    memory: 256Mi
  requests:
    cpu: 100m
    memory: 256Mi
{{- end}}
{{- end -}}
